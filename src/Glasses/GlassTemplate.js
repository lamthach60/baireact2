import React, { Component } from "react";
import "./style.css";
import { data_Glass } from "./dataGlass";
import ItemGlass from "./ItemGlass";
export default class GlassTemplate extends Component {
  state = {
    listGlass: data_Glass,
    img: "./Ex_glasses/glassesImage/v1.png",
    name: "GUCCI G8850U",
    desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  };

  handleGlass = (url, desc, name) => {
    console.log(" desc: ", desc);
    console.log(url);
    this.setState({
      img: url,
      desc: desc,
      name: name,
    });
  };

  renderGlass = () => {
    return this.state.listGlass.map((item) => {
      return (
        <ItemGlass handleGlass={this.handleGlass} glass={item} key={item.id} />
      );
    });
  };

  render() {
    return (
      <>
        <div>
          <div className="bgtags">
            <div className="bgtags_ray">
              <div className="bgtags_header">
                <p>TRY GLASSES APP ONLINE</p>
              </div>
              <div className="container bgtags_model">
                <div className="row">
                  <div className="col-6">
                    <img
                      src="./Ex_glasses/glassesImage/model.jpg"
                      width="450px"
                      height="550px"
                    />
                    <div className="avatar">
                      <img src={this.state.img} alt="fail" />
                    </div>
                    <div className="info_glasses">
                      <h2>{this.state.name}</h2>
                      <p>{this.state.desc}</p>
                    </div>
                  </div>
                  <div className="col-6">
                    <div className="img_glasses">{this.renderGlass()}</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* Optional JavaScript */}
          {/* jQuery first, then Popper.js, then Bootstrap JS */}
        </div>
      </>
    );
  }
}
