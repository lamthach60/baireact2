import React, { Component } from "react";
import { data_Glass } from "./dataGlass";
import GlassTemplate from "./GlassTemplate";
import handleGlass from "./GlassTemplate";
export default class itemGlass extends Component {
  render() {
    // console.log(this.props);
    return (
      <>
        <button
          onClick={() => {
            this.props.handleGlass(
              this.props.glass.url,
              this.props.glass.desc,
              this.props.glass.name
            );
            // this.props.handleGlass(this.props.glass.desc);
          }}
          className="btn btn-glass"
        >
          <img src={this.props.glass.url} width="150px" height="50px" />
        </button>
      </>
    );
  }
}
