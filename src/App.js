import logo from "./logo.svg";
import "./App.css";
import Header from "./Header/Header";
import GlassTemplate from "./Glasses/GlassTemplate";
function App() {
  return (
    <>
      <Header />
      <GlassTemplate />
    </>
  );
}

export default App;
